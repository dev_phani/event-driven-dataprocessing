##################################
## Author - J V Phanindra Reddy ##
##################################

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *
from datetime import datetime
import logging
from google.cloud import logging as stackdriver
#from google.cloud import bigquery
import argparse

job_start_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
logging.basicConfig(format="%(asctime)s-%s(levelname)s-%(message)s",datefmt="%y-%m-%d %H:%M%S")

#set stackdriver logging
sd_client = stackdriver.Client()
sd_logger = sd_client.logger("dataproc-job-error-log")

parser = argparse.ArgumentParser()

parser.add_argument("job_id",help="dataproc job id")
parser.add_argument("file_to_be_processed",help="input file for data processing")
#parser.add_argument("pwd")
#parser.add_argument("ip")
#parser.add_argument("usr")

args = parser.parse_args()

job_id=args.job_id
file_to_be_processed=args.file_to_be_processed






print("creating spark session")

spark = SparkSession.builder.appName("app_batch_data_load").getOrCreate()

print(f"Job id is: {job_id}")
print(f"file_to_be_processed :{file_to_be_processed}")
#print(f"Recieved Args : {input_params}")


#bq_clinet = bigquery.Client()
temp_bucket ="you_temp_bucket"
spark.conf.set('temporaryGcsBucket',temp_bucket)


try:
    input_df = spark.read.parquet(f"gs://{file_to_be_processed}")
except Exception as e:
    logging.error(f"File not found: {e}")
    raise e

try:
    
    input_df = input_df.withColumn("VendorID",input_df.VendorID.cast(StringType()))

    #input_df = input_df.select("")
    
    input_df = input_df.withColumn("payment_method",when(input_df.payment_type==1,"Wallet")\
                                           .when(input_df.payment_type==2,"Paypal")\
                                           .when(input_df.payment_type==3,"Credit card")\
                                           .when(input_df.payment_type==4,"Debit card")\
                                           .when(input_df.payment_type==5,"Cash")\
                                           .otherwise("Others"))
                                           
    
    input_df = input_df.withColumn("trip_duration",unix_timestamp("tpep_dropoff_datetime")-unix_timestamp("tpep_pickup_datetime")/60)
 
    input_df = input_df.select("VendorID","tpep_pickup_datetime","tpep_dropoff_datetime","passenger_count","trip_distance","trip_duration","payment_method","payment_type")
except Exception as e:
    logging.error(f"Error while Transforming data: {e}")
    raise e
   
    
  
try:
    staging_bucket = "staging_bucket"
    print(f"Loading data GCS bucket: {staging_bucket}")
    input_df.write.mode("overwrite").parquet(f"gs://{staging_bucket}/staging/")
    print(f"Finished Loading data into GCS bucket: {bucket}")
    #write to bq
    input_df.write.format("bigquery").option('table','yellow_trip_data.yellow_trip_data_monthly').mode("append").save()
except Exception as e:
    logging.error(f"Error while loading data into Sink: {e}")
    raise e





