resource "google_bigquery_dataset" "yellow-trip" {
  dataset_id                  = "yellow_trip"
  friendly_name               = "yellow_trip"
  description                 = "To Store yellow trips data"
  location                    = "US"

  labels = {
    owner = "phani",
    team = "analytics"
  }
}

#table
resource "google_bigquery_table" "tbl-1" {
  dataset_id = google_bigquery_dataset.yellow-trip.dataset_id
  table_id   = "yellow_trip_streaming"

  time_partitioning {
    type = "DAY"
    field = "df_proc_time"
  }

  labels = {
    env = "default"
  }

  schema = file("yellow_trip_streaming_schema.json")

}
