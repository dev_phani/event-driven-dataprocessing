resource "google_storage_bucket_object" "zip" {
    source       = "src.zip"
    content_type = "application/zip"

    # Append to the MD5 checksum of the files's content
    # to force the zip to be updated as soon as a change occurs
    name         = "src.zip"
    bucket       = "temp-2001"
}

# Create the Cloud function triggered by a `Finalize` event on the bucket
resource "google_cloudfunctions_function" "function" {
    name                  = "trigger_batch_load_dag"
    runtime               = "python37"  # of course changeable
    region = var.region
    labels = {
    owner = "phani",
    team="analytis"
             }
    # Get the source code of the cloud function as a Zip compression
    source_archive_bucket = "temp-2001"
    source_archive_object = google_storage_bucket_object.zip.name

    # Must match the function name in the cloud function `main.py` source code
    entry_point           = "trigger_batch_load_dag"

    
    # 
    event_trigger {
        event_type = "google.storage.object.finalize"
        resource   = "temp-2001"
    }
}
