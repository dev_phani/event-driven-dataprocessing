resource "google_storage_bucket" "landing_bucket" {
  name          = "auto-expiring-bucket-phani"
  location      = "US"
  force_destroy = true
  storage_class="STANDARD"

  lifecycle_rule {
    condition {
      age = 3
    }
    action {
      type = "SetStorageClass"
	  storage_class="ARCHIVE"
    }
  }

  lifecycle_rule {
    condition {
      age = 1
    }
    action {
      type = "SetStorageClass"
	  storage_class="COLDLINE"
    }
  }
}