resource "google_pubsub_topic" "tp_1" {
  name = "yellow-trip-topic"

  labels = {
    owner = "phani",
    team="analytis"
  }
}

resource "google_pubsub_subscription" "tp_1_sp" {
  name  = "yellow-trip-sub"
  topic = google_pubsub_topic.tp_1.name

  ack_deadline_seconds = 20

  labels = {
    owner = "phani",
    team="analytis"
  }
}
