resource "google_composer_environment" "env_1" {
  name   = "airflow-dev"
  region = var.region
  labels = {
    owner = "phani",
    team="analytis"
  }
  config {
    node_count = 3

    software_config {
      image_version = "composer-1-airflow-2"
    }

    node_config {
      zone         = "us-central1-a"
      machine_type = "n1-standard-1"
    }

  }
}
