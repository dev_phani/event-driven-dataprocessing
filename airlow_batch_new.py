##################################
## Author - J V Phanindra Reddy ##
##################################

from datetime import datetime
import os
import random
import pendulum
import airflow
import logging
from airflow.models import DAG,Variable
from airflow.hooks.base_hook import BaseHook
from google.cloud import logging as stackdriver
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.providers.google.cloud.operators.dataproc import (
    DataprocCreateClusterOperator,
    DataprocDeleteClusterOperator,
    DataprocSubmitJobOperator,
    ClusterGenerator,
)
from airflow.contrib.operators.dataproc_operator import DataProcPySparkOperator,DataprocClusterCreateOperator

logging.basicConfig(format="%(asctime)s-%(levelname)s-%(message)s",datefmt="%Y-%m-%d %H:%M:%S")
sd_client = stackdriver.Client()
sd_logger = sd_client.logger("composer-error-log")


#getting airlfow vars
project_id = Variable.get("project_id")
region=Variable.get("region")
cluster_config=Variable.get("cluster_config",deserialize_json=True)
pyspark_job_config=Variable.get("pyspark_job_config",deserialize_json=True)
#get pwd





#Methods

def create_dataproc_cluster(**context):
    """
    This method will create a DataProc cluster
    with initialization actions which will
    enable dataproc to connect to bigquery
    """
    try:
        #conn = BaseHook.get_connection('my_sql_instance_conn')

        #print(conn.get_extra())
        #print(f"Cluster config:{cluster_config}")
        #print("Generating Cluster Config")
        cluster_name="pyspark-cluster-"+''.join(random.sample("123456789",4))
        cluster_config_gen = ClusterGenerator(
                             project_id=project_id,
                             region=region,
                             master_machine_type= cluster_config['master_config']['machine_type_uri'],
                             worker_machine_type= cluster_config['worker_config']['machine_type_uri'],
                             master_disk_size=cluster_config['master_config']["disk_config"]["boot_disk_size_gb"],
                             worker_disk_size=cluster_config['worker_config']["disk_config"]["boot_disk_size_gb"],  
                             num_workers = cluster_config['worker_config']['num_instances'],
                             metadata =cluster_config["metadata"],
                             init_actions_uris=cluster_config["init_actions"],
                             image_version=cluster_config["image_version"]
        ).make()
        print(f"Creating cluster: {cluster_name}")
        task_instance=context['ti']
        print(f"Cluster config:{cluster_config_gen}")
        #sd_logger.log_text(f"Creating cluster: {cluster_name}",severity=INFO)

        tsk = DataprocCreateClusterOperator(
        task_id = "create_dataproc_cluster",
        project_id = project_id,
        cluster_name = cluster_name,
        cluster_config=cluster_config_gen,
        region = region,
        )
        tsk.execute(context)

        #tsk.execute(context)
        print(f"Created Cluster - {cluster_name} Successfully")
        task_instance.xcom_push(key="cluster_created",value=cluster_name)
    except Exception as e:
        sd_logger.log_text(f"Error while creating dataproc cluster: {e}",severity="ERROR")
        logging.error(f"Error while creating dataproc cluster: {e}")
        raise e 
        
def submit_dataproc_job(**context):
    """
    This method submits dataproc job
    on dataproc cluster with input file 
    and jar file
    """
    try:
        task_instance = context['ti']
        cluster_created = task_instance.xcom_pull(key="cluster_created",task_ids=['create_dataproc_cluster'])[0]
        job_name = "yellow-trip-batch-load-"+datetime.now().strftime("%y%m%d%H%M%S")
        print(f"Submitting DataProc job - {job_name} on Cluster - {cluster_created}")
        raw_file_id = context['dag_run'].conf['id'].split("/")
        raw_file_id.pop(-1)
        file_to_be_processed = '/'.join(raw_file_id)
        print(f"File to be processed: {file_to_be_processed}")

        tsk = DataProcPySparkOperator(
        task_id = "submit_dataproc_job",
        #job=pyspark_job_config,
        main = pyspark_job_config['pyspark_job']['main_python_file_uri'],
        job_name = job_name,
        region = region,
        cluster_name = cluster_created,
        dataproc_jars = ["gs://temp-2001/spark-bigquery-with-dependencies_2.12-0.22.2.jar"],
        arguments = [job_name,file_to_be_processed],
        project_id = project_id,)

        tsk.execute(context)
        job_end_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(f"{job_name} completed Successfully")
    except Exception as e:
        sd_logger.log_text(f"Error while submitting dataproc job - {job_name}: {e}",severity="ERROR")
        logging.error(f"Error while submitting dataproc job - {job_name}: {e}")
        raise e
        
def delete_dataproc_cluster(**context):
    """
    This method will deleted the created cluster
    """
    try:
        task_instance = context['ti']
        cluster_created = task_instance.xcom_pull(key="cluster_created",task_ids=['create_dataproc_cluster'])[0]
        print("deleting the cluster - {cluster_name}")

        tsk = DataprocDeleteClusterOperator(
        task_id = "delete_dataproc_cluster",
        project_id = project_id,
        region = region,
        cluster_name = cluster_created)

        tsk.execute(context)
    except Exception as e:
        sd_logger.log_text(f"Error while deleting dataproc cluster: {e}",severity="ERROR")
        logging.error(f"Error while deleting dataproc cluster: {e}")
        raise e
    



##############-------------DAG-------------------#########################
 
dag = DAG(dag_id="load_yellow_trip_batch",schedule_interval=None,start_date=pendulum.datetime(2021, 1, 1, tz="UTC")) 
    
start = DummyOperator(task_id='start',dag=dag)


create_dataproc_cluster = PythonOperator(
    task_id='create_dataproc_cluster', 
    dag=dag,
    python_callable=create_dataproc_cluster,
    provide_context=True)

submit_dataproc_job = PythonOperator(
    task_id='submit_dataproc_job',
    dag=dag,
    python_callable=submit_dataproc_job,
    provide_context=True)

delete_dataproc_cluster = PythonOperator(
    task_id='delete_dataproc_cluster',
    dag=dag,
    python_callable=delete_dataproc_cluster,
    provide_context=True)

end = DummyOperator(task_id='end',dag=dag)

start>>create_dataproc_cluster>>submit_dataproc_job>>delete_dataproc_cluster>>end
